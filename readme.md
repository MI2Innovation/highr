# Highr

Write your FHIR&reg; app, leave the medical taxonomy codes to us

## Description

Highr gives you the medical taxonomy codes for common medical conditions, medications, and procedures. <br><br>&#128516; We also give you their provenance, and how well they work in published studies.

### How to use

To get available definitions, 

```
https://qgn6n3pu8f.execute-api.us-east-1.amazonaws.com/dev/definitions
```

To ask a specific question, urlencode the question with the resource in the call such as 

```
$.ajax({
          url:"https://qgn6n3pu8f.execute-api.us-east-1.amazonaws.com/dev/v1?Question="+urlencodedQuestionFromDefinition+"&Resource="+Resource,
          headers:{
            "Content-Type":"application/x-www-form-urlencoded",
            "x-api-key":"FWc30RFVrl8ct4QA0BXLKanvgdY74uKO2aL3dkEU"
          },
          success:function(data){ renderResult(data) },
          error:function(){ console.log(data) },
        })
```

the x-api-key is a sample key and limited to 1000 requests/day, please contact us for your own key
